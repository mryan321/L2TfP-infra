variable project {
  type = "string"
}

variable region {
  type = "string"
}

variable zone {
  type = "string"
}

variable domain {
  type = "string"
}

variable envname {
  type = "string"
}

variable global_address_network_tier {
  type = "string"
}

variable gke_machine_type {
  type = "string"
}

variable gke_initial_node_count {
  type = "string"
}

variable gke_nodepool_machine_type {
  type = "string"
}

variable gke_nodepool_min_node_count {
  type = "string"
}

variable gke_nodepool_max_node_count {
  type = "string"
}