# DNS Zone
resource "google_dns_managed_zone" "external" {
  name        = "${var.envname}-external"
  dns_name    = "${var.domain}."
  description = "DNS zone created and managed by Terraform."
}

# DNS: www
resource "google_dns_record_set" "www" {
  name         = "www.${var.domain}."
  managed_zone = "${google_dns_managed_zone.external.name}"
  type         = "A"
  ttl          = 300
  rrdatas      = ["${google_compute_global_address.www.address}"]
}

# GLOBAL IP: www
resource "google_compute_global_address" "www" {
  name = "${var.envname}-www"
  # network_tier = "${var.global_address_network_tier}" support soon: https://github.com/terraform-providers/terraform-provider-google/issues/1583#issuecomment-394850055
}