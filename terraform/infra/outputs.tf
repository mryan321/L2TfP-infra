output "domain" {
  value = "${var.domain}"
}

output "www_address" {
  value = "${google_compute_global_address.www.address}"
}
