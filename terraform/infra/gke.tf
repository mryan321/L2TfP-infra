resource "google_container_cluster" "gke-cluster" {
  name               = "${var.envname}-gke-cluster"
  project            = "${var.project}"
  network            = "default"
  zone               = "${var.zone}"
  initial_node_count = "${var.gke_initial_node_count}"

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

# ITS SLOW CREATING NODE POOL ON GCP - its a ballache when youre applying and destroying a lot!
#resource "google_container_node_pool" "default_pool" {
#  name    = "${var.envname}-node-pool"
#  zone    = "${var.zone}"
#  cluster = "${google_container_cluster.gke-cluster.name}"
#  initial_node_count = "1"
#
#  node_config {
#    preemptible  = true
#    machine_type = "${var.gke_nodepool_machine_type}"
#
#    oauth_scopes = [
#      "https://www.googleapis.com/auth/compute",
#      "https://www.googleapis.com/auth/devstorage.read_only",
#      "https://www.googleapis.com/auth/logging.write",
#      "https://www.googleapis.com/auth/monitoring",
#    ]
#
#    labels {
#      service = "default"
#    }
#
#    tags = ["default"]
#  }
#
#  autoscaling = {
#    min_node_count = "${var.gke_nodepool_min_node_count}"
#    max_node_count = "${var.gke_nodepool_max_node_count}"
#  }
#
#  management = {
#    auto_repair  = true
#    auto_upgrade = false
#  }
#}
