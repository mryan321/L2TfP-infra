provider "google" {
  credentials = "${file("terraform/credentials/ci-svc-acct.json")}"
  project     = "${var.project}"
  region      = "${var.region}"
}