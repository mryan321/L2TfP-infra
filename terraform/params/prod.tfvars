# Environment
project = "l2tfp111"
region = "europe-west1"
zone = "europe-west1-b"
domain = "l2t4p.com"
envname = "prod"

# Networking
global_address_network_tier = "PREMIUM"

# GKE sizing
gke_machine_type = "n1-standard-1"
gke_initial_node_count = "3"  # make 1 when node pool implemented
gke_nodepool_machine_type = "n1-standard-1"
gke_nodepool_min_node_count = "1"
gke_nodepool_max_node_count = "3"