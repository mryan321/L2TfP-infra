# Environment
project = "l2tfp000"
region = "europe-west1"
zone = "europe-west1-b"
domain = "l2t4p-dev.com"
envname = "dev"

# Networking
global_address_network_tier = "STANDARD"

# GKE sizing
gke_machine_type = "n1-standard-1"
gke_initial_node_count = "3" # make 1 when node pool implemented
gke_nodepool_machine_type = "n1-standard-1"
gke_nodepool_min_node_count = "1"
gke_nodepool_max_node_count = "3"