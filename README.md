# Infrastructure for Learn to Type for Programmers

This will be made up of a series of applications to hopefully demonstrate my skills! I wanted something simple but with enough depth. Its simple enough that I should be able to get something done in a day or so. But the depth comes when you start thinking about how the scraper will work - NLP possibly, ignorning keywords, identifying language.

I'm using Trello for project management, if you would like to see my board please request an invite :)

## What

A simple application that teaches programmers to touch type. It will do this by scraping a GitHub repository and using the results to return back to the user. I thought this might be good because I myself don't touch type but kinda would like to! Some examples of learning to type resources: 
- http://touchtyping.guru/learn-touch-typing 
- https://www.ratatype.com/

## Why

To demonstrate skills in the following:
- Java - this is my bread and butter so will be using this to test new Java features.
- Frontend - I'm not a frontend dev but would like to experiment.
- DevOps - will have CI/CD pipelines configured. All infrastructure will be as code.
- GCP - originally focused on GCP but maybe migrating over to AWS or Azure one day.
- Architecture - will demonstrate a good understanding of modern architectures.
- New Tech - I will continually update with new features to show off the latest and greatest. Generally this will be my playground!

# Infrastructure

So this is the infrastructure repo. The infrastructure should be completly automated. Will use Terraform and Gitlab CI. This allows me to bring it up and tear it down without incurring unknown GCP costs!

## Thanks

I am learning from other peoples work so here is a few resources I found particularly helpful:

- https://cloud.google.com/kubernetes-engine/docs/tutorials/http-balancer
- https://medium.com/@timhberry/learn-terraform-by-deploying-a-google-kubernetes-engine-cluster-a29071d9a6c2

## Testing Locally

This might be a bit rough around the edges as I'm preparing this document after the events!

- copy service account credentials to: 
- rename terraform.tf to terraform.tf.bak - this means terraform will use local state file
- Install Terraform and Google Cloud SDK
- Install kubectl via `gcloud componenents install kubectl`
- Setup a project in GCP manually via https://console.google.com
- at project root run plan: `terraform plan -var-file=terraform/params/dev.tfvars terraform/infra`
- all good then run apply: `terraform apply -var-file=terraform/params/dev.tfvars terraform/infra`
- check console to see gke cluster health - it may take several mins to provision
- once up setup gcloud project: `gcloud config set project l2tfp000` 
- setup kubectl: `gcloud container clusters get-credentials dev-gke-cluster --zone europe-west1-b`
- check nodes: `kubectl get nodes`
- cd into $root/testing/gke dir and run: `kubectl create -f .`
- check pods, services and ingresses: `kubectl get po,svc,ing`
- the ingress address should set to the static ip address in GCP
- add entry to hosts file pointing domain to this ip
- open domain in browser (it might take some time for DNS to update), you'll get a plaintext response like:
```
  Hello, world!
  Version: 1.0.0
  Hostname: testing-64ff9b7878-crcmz
```
- cleanup k8s: `kubectl delete -f .` 
- cleanup gcp (on project root): `terraform destroy -var-file=terraform/params/dev.tfvars terraform/infra`

## Manual Steps Required

At least until I work out an automated way...

- From my laptop(logged in as me(owner): mryan321@gmail.com): `kubectl create clusterrolebinding owner-cluster-admin-binding --clusterrole cluster-admin --user ci-542@l2tfp000.iam.gserviceaccount.com`